---
backup:
    files:
    - /etc/network/interfaces
    - /etc/frr/frr.conf
    - /etc/frr/daemons
    path: ../inventories/evpn_l2only/config/spine01
bgp:
    address_family:
    -   name: ipv4_unicast
        redistribute:
        -   type: connected
    -   name: l2vpn_evpn
        neighbors:
        -   activate: true
            interface: underlay
    asn: '65100'
    neighbors:
    -   interface: swp1
        peergroup: underlay
        unnumbered: true
    -   interface: swp2
        peergroup: underlay
        unnumbered: true
    -   interface: swp3
        peergroup: underlay
        unnumbered: true
    -   interface: swp4
        peergroup: underlay
        unnumbered: true
    -   interface: swp5
        peergroup: underlay
        unnumbered: true
    -   interface: swp6
        peergroup: underlay
        unnumbered: true
    peergroups:
    -   name: underlay
        remote_as: external
    router_id: 10.10.10.101
bgp_asn_prefix: 651
discovered_interpreter_python: /usr/bin/python
dns:
    domain: cumulusnetworks.com
    search_domain:
    - cumulusnetworks.com
    servers:
        ipv4:
        - 1.1.1.1
        - 8.8.8.8
        vrf: mgmt
eth0:
    ips:
    - 192.168.200.21/24
eth0_id: 21
eth0_ip: 192.168.200.21/24
eth0_subnet: 192.168.200.0/24
fabric_name: evpn_l2only
group_names:
- pod1
- spine
groups:
    all:
    - netq-ts
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server04
    - server05
    border:
    - border01
    - border02
    fw:
    - fw1
    leaf:
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    netq:
    - netq-ts
    pod1:
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server04
    - server05
    server:
    - server01
    - server02
    - server04
    - server05
    spine:
    - spine01
    - spine02
    - spine03
    - spine04
    ungrouped: []
id: 101
interfaces:
-   name: swp1
    options:
        extras:
        - alias to leaf
-   name: swp2
    options:
        extras:
        - alias to leaf
-   name: swp3
    options:
        extras:
        - alias to leaf
-   name: swp4
    options:
        extras:
        - alias to leaf
-   name: swp5
    options:
        extras:
        - alias to leaf
-   name: swp6
    options:
        extras:
        - alias to leaf
inventory_dir: /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only
inventory_file: /home/cumulus/cumulus_ansible_modules/inventories/evpn_l2only/hosts
inventory_hostname: spine01
inventory_hostname_short: spine01
leaf_spine_interface:
    extras:
    - alias to spine
loopback:
    ips:
    - 10.10.10.101/32
mlagBondProfileA:
    bridge_access: 10
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileB:
    bridge_access: 20
    extras:
    - bond-lacp-bypass-allow yes
    - mstpctl-bpduguard yes
    - mstpctl-portadminedge yes
mlagBondProfileFW:
    bridge_vids:
    - 10
    - 20
mlag_sysmac_prefix: 44:38:39:BE:EF
netq:
    agent_server: 192.168.200.250
    cli_access_key: long-key-0987654321
    cli_port: 443
    cli_premises: CITC
    cli_secret_key: long-key-1234567890
    cli_server: api.air.netq.cumulusnetworks.com
    version: latest
ntp:
    server_ips:
    - 0.cumulusnetworks.pool.ntp.org
    - 1.cumulusnetworks.pool.ntp.org
    - 2.cumulusnetworks.pool.ntp.org
    - 3.cumulusnetworks.pool.ntp.org
    timezone: America/Los_Angeles
playbook_dir: /home/cumulus/cumulus_ansible_modules/playbooks
snmp:
    addresses:
    - 192.168.200.21@mgmt
    - udp6:[::1]:161@mgmt
    rocommunity:
    - public
    - private
spine_leaf_interface:
    extras:
    - alias to leaf
ssh:
    banner: |-
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Authorized Users Only!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    motd: |-
        #########################################################
        Successfully logged in to: spine01
        #########################################################
syslog:
    servers:
    - 192.168.200.1
tacacs:
    groups:
    -   name: admins
        priv_level: 15
    -   name: basics
        priv_level: 1
    secret: tacacskey
    server_ips:
    - 192.168.200.1
    users:
    -   group: basics
        name: basicuser
        password: password
    -   group: admins
        name: adminuser
        password: password
    vrf: mgmt
vlan10:
    extras:
    - ip-forward off
    - ip6-forward off
    id: 10
    name: vlan10
vlan20:
    extras:
    - ip-forward off
    - ip6-forward off
    id: 20
    name: vlan20
vni10:
    bridge:
        access: 10
    name: vni10
    vxlan_id: 10
vni20:
    bridge:
        access: 20
    name: vni20
    vxlan_id: 20
vrf_mgmt:
    extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vrfs:
-   extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vxlan_anycast_loopback: 10.0.1.101/32
vxlan_local_loopback: 10.10.10.101/32
...
